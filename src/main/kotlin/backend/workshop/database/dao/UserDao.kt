package backend.workshop.database.dao

import backend.workshop.database.entity.User

interface UserDao : CommonDao<User>
