package backend.workshop.model.mapper

import backend.workshop.database.entity.User
import backend.workshop.model.request.UserRequest
import backend.workshop.model.response.UserResponse
import org.springframework.stereotype.Component

@Component
class UserMapper {
    fun asEntity(request: UserRequest) = User(
        name = request.name,
        email = request.email,
    )

    fun asResponse(user: User) = UserResponse(
        id = user.id,
        createdAt = user.createdAt,
        name = user.name,
        email = user.email,
    )

    fun update(user: User, request: UserRequest): User {
        user.name = request.name
        user.email = request.email
        return user
    }

    fun asListResponse(users: Iterable<User>) = users.map { asResponse(it) }
}
