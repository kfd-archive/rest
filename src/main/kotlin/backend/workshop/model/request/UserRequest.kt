package backend.workshop.model.request

class UserRequest(
    val name: String,
    val email: String,
)
