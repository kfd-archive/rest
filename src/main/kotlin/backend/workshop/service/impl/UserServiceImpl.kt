package backend.workshop.service.impl

import backend.workshop.database.dao.UserDao
import backend.workshop.model.exception.NotFoundException
import backend.workshop.model.mapper.UserMapper
import backend.workshop.model.request.UserRequest
import backend.workshop.model.response.UserResponse
import backend.workshop.service.UserService
import org.springframework.data.jpa.repository.Modifying
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class UserServiceImpl(
    private val dao: UserDao,
    private val mapper: UserMapper,
) : UserService {
    override fun list(): List<UserResponse> {
        val users = dao.findAll()
        return mapper.asListResponse(users)
    }

    override fun getById(id: Long): UserResponse {
        val user = dao.findEntityById(id) ?: throw NotFoundException()
        return mapper.asResponse(user)
    }

    @Transactional
    override fun create(request: UserRequest): UserResponse {
        val user = mapper.asEntity(request).apply { dao.save(this) }
        return mapper.asResponse(user)
    }

    @Transactional
    @Modifying
    override fun update(id: Long, request: UserRequest): UserResponse {
        val user = dao.findEntityById(id) ?: throw NotFoundException()
        val updated = mapper.update(user, request)
        return mapper.asResponse(updated)
    }

    @Transactional
    @Modifying
    override fun delete(id: Long) {
        val user = dao.findEntityById(id) ?: throw NotFoundException()
        dao.delete(user)
    }
}
